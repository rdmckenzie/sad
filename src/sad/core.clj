(ns sad.core
  (:require ;; This codebase
            [sad.bnf  :as bnf]
            [sad.ebnf :as ebnf]
            [sad.abnf :as abnf]
            ;; 3rd party tools
            [clojure.tools.cli :only [cli] :as tools]))

(defn -main
  [& args]
  (let [[options trailing-args banner]
        (tools/cli args 
             ["-f" "--file" "The BNF source file" :default nil]
             ["--ebnf" :default false :flag true]
             ["--abnf" :default false :flag true])]
    (let [grammar-file (or (:file options) 
                           (first trailing-args))]
      (cond
        (and (empty? trailing-args)
             (nil? (get options :file)))
            (println "Fatal error, no grammar file specified.")

        (get options :ebnf)
            (ebnf/build grammar-file)

        (get options :abnf)
            (abnf/build grammar-file)

        :else (bnf/build grammar-file)))))
