(ns sad.bnf
  (:require [name.choi.joshua.fnparse :as fnp]
            [sad.common :as common]))

;------------------------------------------------------------------------------
; Define the BNF syntax
;------------------------------------------------------------------------------
(declare syntax rule rule-name literal opt-whitespace expression line-end blist 
         term literal)

(def syntax     
  (fnp/rep+ rule))

(def rule       
  (fnp/conc 
    opt-whitespace 
    (fnp/lit-conc-seq [":" ":" "="])
    opt-whitespace 
    expression
    line-end))

(def expression 
  (fnp/rep+ 
    (fnp/conc 
     blist 
      (fnp/opt 
        (fnp/lit "|")))))

(def blist       
  (fnp/rep+ term))

(def term       
  (fnp/alt rule-name literal))

(def rule-name      
  (fnp/semantics
    (fnp/conc
      (fnp/lit "<") 
      (fnp/re-term #"[\w\-]+")
      (fnp/lit ">"))
    symbol))

(def literal
  (fnp/semantics 
    (fnp/re-term #"[#\"\\]*(?:\\.[^\"\\]*)*")
    re-pattern))

(def opt-whitespace 
  (fnp/rep* 
    (fnp/alt 
      (fnp/lit " ") 
      (fnp/lit "\t"))))

(def line-end       
  (fnp/constant-semantics 
    (fnp/rep* 
      (fnp/lit-alt-seq ["\n" "\r" "\f"])) 
    nil))

;------------------------------------------------------------------------------
; Define the compiler

(def document
  (remove
    nil?
    (fnp/rep+
      (fnp/alt
        (fnp/constant-semantics 
          (fnp/alt opt-whitespace
                   line-end) nil)
        rule))))

(defn build [file]
  (common/parse document (slurp file)))

