(ns sad.common
  (:require [name.choi.joshua.fnparse :as fnp]))

(def split-regex #"(\"(?:[^\"\\]|\\.)*\")|(\S+)")

(defn parse [grammar string &{:keys [error incomplete]
                              :or   {:error prn
                                     :incomplete prn}}]
  (fnp/rule-match grammar 
                  error 
                  incomplete 
                  {:remainder (map first
                                  (re-seq split-regex 
                                          string))}))
